
package com.aeolus.vidmeplay.Model.Net.retrofitModel;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Video {

    @SerializedName("video_id")
    @Expose
    private String videoId;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("height")
    @Expose
    private Integer height;

    @SerializedName("width")
    @Expose
    private Integer width;

    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;

    @SerializedName("likes_count")
    @Expose
    private Integer likesCount;

    @SerializedName("complete_url")
    @Expose
    private String completeUrl;

    public String getVideoId() {
        return videoId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Integer getHeight() {
        return height;
    }

    public Integer getWidth() {
        return width;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public String getCompleteUrl() {
        return completeUrl;
    }

}
