package com.aeolus.vidmeplay.Model.Net;

import com.aeolus.vidmeplay.Model.Net.retrofitModel.AuthModel;
import com.aeolus.vidmeplay.Model.Net.retrofitModel.VidmeVideosList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Yuriy on 29.09.2017.
 */

interface VidmeApi {

    @GET("/videos/featured")
    Call<VidmeVideosList> getFeaturedVideos(@Query("offset") Integer offset, @Query("limit") Integer limit);

    @GET("/videos/new")
    Call<VidmeVideosList> getNewVideos(@Query("offset") Integer offset, @Query("limit") Integer limit);

    @POST("/auth/create")
    Call<AuthModel> getAuth(@Query("username") String username, @Query("password") String password);

    @GET("videos/feed")
    Call<VidmeVideosList> getFollowedVideos(@Header("AccessToken") String token, @Query("offset") Integer offset, @Query("limit") Integer limit);
}
