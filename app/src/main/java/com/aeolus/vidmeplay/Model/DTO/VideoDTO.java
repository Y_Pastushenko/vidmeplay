package com.aeolus.vidmeplay.Model.DTO;

/**
 * Created by Yuriy on 25.10.2017.
 */

public class VideoDTO {
    private final String videoId;
    private final String thumbnailUrl;
    private final String completeUrl;
    private final String description;
    private final String title;
    private final Integer likesCount;
    private final Integer videoHeight;
    private final Integer videoWidth;

    public VideoDTO(String videoId, String thumbnailUrl, String completeUrl, String description, String title, Integer likesCount, int videoHeight, int videoWidth) {
        this.videoId = videoId;
        this.thumbnailUrl = thumbnailUrl;
        this.completeUrl = completeUrl;
        this.description = description;
        this.title = title;
        this.likesCount = likesCount;
        this.videoHeight = videoHeight;
        this.videoWidth = videoWidth;
    }

    public String getVideoId() {
        return videoId;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getCompleteUrl() {
        return completeUrl;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public int getVideoHeight() {
        return videoHeight;
    }

    public int getVideoWidth() {
        return videoWidth;
    }

}