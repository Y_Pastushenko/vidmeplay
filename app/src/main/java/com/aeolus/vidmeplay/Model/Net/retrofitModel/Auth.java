package com.aeolus.vidmeplay.Model.Net.retrofitModel;

/**
 * Created by Yuriy on 27.10.2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Auth {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("expires")
    @Expose
    private String expires;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getToken() {
        return token;
    }

}