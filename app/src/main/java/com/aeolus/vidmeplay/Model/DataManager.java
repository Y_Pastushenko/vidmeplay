package com.aeolus.vidmeplay.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.aeolus.vidmeplay.Model.DTO.VideoDTO;
import com.aeolus.vidmeplay.Model.Net.NetManager;
import com.aeolus.vidmeplay.Presenter.IMainPagePresenter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Yuriy on 24.10.2017.
 */

public class DataManager implements IDataManager {
    private static IMainPagePresenter presenter;
    private static final DataManager instance = new DataManager();
    private static NetManager netManager;
    private static final String SHARED_PREFERENCES_NAME = "com.aeolus.vidmeplay.shared.preferences";
    private static final String TOKEN_KEY = "token_key";

    private DataManager() {
    }

    public static DataManager getInstance(IMainPagePresenter presenter) {
        DataManager.presenter = presenter;
        if (netManager == null)
            netManager = new NetManager(instance);
        return instance;
    }

    @Override
    public Observable<List<VideoDTO>> getFeaturedVideos(int offset, int limit) {
        return netManager.getFeaturedVideos(offset, limit);
    }

    @Override
    public Observable<List<VideoDTO>> getNewVideos(int offset, int limit) {
        return netManager.getNewVideos(offset, limit);
    }

    @Override
    public void logIn(String username, String password) {
        netManager.getAuth(username, password).subscribe(auth -> {
            saveToken(auth.getToken());
        });
    }

    @Override
    public void logOut() {
        saveToken("");
    }

    @Override
    public Observable<List<VideoDTO>> getFollowedVideos(int offset, int limit) {
        if (getToken().length() == 0)
            return Observable.just(new ArrayList<>());
        return netManager.getFollowedVideos(getToken(), offset, limit);
    }

    @Override
    public void onLoginSuccessful() {
        presenter.onLoginSuccessful();
    }

    @Override
    public void onInvalidUsername() {
        presenter.onInvalidUsername();
    }

    @Override
    public void onInvalidPassword() {
        presenter.onInvalidPassword();
    }

    @Override
    public boolean isLoggedIn() {
        return getToken().length() > 0;
    }

    private void saveToken(String token) {
        SharedPreferences preferences = presenter.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TOKEN_KEY, token);
        editor.apply();
    }

    private String getToken() {
        SharedPreferences preferences = presenter.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return preferences.getString(TOKEN_KEY, "");
    }

    @Override
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) presenter.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onNetworkLost() {
        presenter.onNetworkNotAvailable();
    }
}
