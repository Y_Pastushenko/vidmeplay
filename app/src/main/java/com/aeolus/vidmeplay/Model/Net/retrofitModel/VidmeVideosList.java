
package com.aeolus.vidmeplay.Model.Net.retrofitModel;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VidmeVideosList {

    @SerializedName("videos")
    @Expose
    private final List<Video> videos = null;

    public List<Video> getVideos() {
        return videos;
    }

}
