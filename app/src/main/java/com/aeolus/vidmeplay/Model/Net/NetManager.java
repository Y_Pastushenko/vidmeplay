package com.aeolus.vidmeplay.Model.Net;

import com.aeolus.vidmeplay.Model.DTO.VideoDTO;
import com.aeolus.vidmeplay.Model.IDataManager;
import com.aeolus.vidmeplay.Model.Net.retrofitModel.Auth;
import com.aeolus.vidmeplay.Model.Net.retrofitModel.AuthModel;
import com.aeolus.vidmeplay.Model.Net.retrofitModel.Video;
import com.aeolus.vidmeplay.Model.Net.retrofitModel.VidmeVideosList;
import com.annimon.stream.Stream;

import org.json.JSONObject;

import java.net.UnknownHostException;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yuriy on 29.09.2017.
 */

public class NetManager {

    private final IDataManager dataManager;

    private VidmeApi vidmeApi;
    private Retrofit retrofit;

    public NetManager(IDataManager dataManager) {
        this.dataManager = dataManager;
    }

    private VidmeApi getVidmeApi() {
        if (vidmeApi == null || retrofit == null) {
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl("https://api.vid.me/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            vidmeApi = retrofit.create(VidmeApi.class);
        }
        return vidmeApi;
    }

    public Observable<Auth> getAuth(String username, String password) {
        return Observable.create(emitter -> {
            VidmeApi api = getVidmeApi();
            api.getAuth(username, password).enqueue(new Callback<AuthModel>() {
                @Override
                public void onResponse(Call<AuthModel> call, Response<AuthModel> response) {
                    if (response.isSuccessful()) {
                        Auth auth = response.body().getAuth();
                        emitter.onNext(auth);
                        emitter.onComplete();
                        dataManager.onLoginSuccessful();
                    } else {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            switch (jObjError.getString("code")) {
                                case "invalid_password":
                                    dataManager.onInvalidPassword();
                                    break;
                                case "invalid_username":
                                    dataManager.onInvalidUsername();
                                    break;
                            }
                            emitter.onComplete();
                        } catch (Exception e) {
                            emitter.onComplete();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AuthModel> call, Throwable t) {
                    emitter.onComplete();
                    if (t instanceof UnknownHostException)
                        dataManager.onNetworkLost();
                }
            });
        });
    }

    public Observable<List<VideoDTO>> getFeaturedVideos(int offset, int limit) {
        return Observable.create(emitter -> {
            VidmeApi api = getVidmeApi();
            api.getFeaturedVideos(offset, limit).enqueue(new Callback<VidmeVideosList>() {
                @Override
                public void onResponse(Call<VidmeVideosList> call, Response<VidmeVideosList> response) {
                    if (response.isSuccessful()) {
                        List<Video> videos = response.body().getVideos();
                        if (videos != null)
                            emitter.onNext(Stream.of(videos).map(v -> new VideoDTO(
                                    v.getVideoId(),
                                    v.getThumbnailUrl(),
                                    v.getCompleteUrl(),
                                    v.getDescription(),
                                    v.getTitle(),
                                    v.getLikesCount(),
                                    v.getHeight(),
                                    v.getWidth())).toList());
                        emitter.onComplete();
                    } else {
                        emitter.onComplete();
                    }
                }

                @Override
                public void onFailure(Call<VidmeVideosList> call, Throwable t) {
                    emitter.onComplete();
                    if (t instanceof UnknownHostException)
                        dataManager.onNetworkLost();
                }
            });
        });
    }

    public Observable<List<VideoDTO>> getNewVideos(int offset, int limit) {
        return Observable.create(emitter -> {
            VidmeApi api = getVidmeApi();
            api.getNewVideos(offset, limit).enqueue(new Callback<VidmeVideosList>() {
                @Override
                public void onResponse(Call<VidmeVideosList> call, Response<VidmeVideosList> response) {
                    if (response.isSuccessful()) {
                        List<Video> videos = response.body().getVideos();
                        if (videos != null)
                            emitter.onNext(Stream.of(videos).map(v -> new VideoDTO(
                                    v.getVideoId(),
                                    v.getThumbnailUrl(),
                                    v.getCompleteUrl(),
                                    v.getDescription(),
                                    v.getTitle(),
                                    v.getLikesCount(),
                                    v.getHeight(),
                                    v.getWidth())).toList());
                        emitter.onComplete();
                    } else {
                        emitter.onComplete();
                    }
                }

                @Override
                public void onFailure(Call<VidmeVideosList> call, Throwable t) {
                    emitter.onComplete();
                    if (t instanceof UnknownHostException)
                        dataManager.onNetworkLost();
                }
            });
        });
    }

    public Observable<List<VideoDTO>> getFollowedVideos(String token, int offset, int limit) {
        return Observable.create(emitter -> {
            VidmeApi api = getVidmeApi();
            api.getFollowedVideos(token, offset, limit).enqueue(new Callback<VidmeVideosList>() {
                @Override
                public void onResponse(Call<VidmeVideosList> call, Response<VidmeVideosList> response) {
                    if (response.isSuccessful()) {
                        List<Video> videos = response.body().getVideos();
                        if (videos != null)
                            emitter.onNext(Stream.of(videos).map(v -> new VideoDTO(
                                    v.getVideoId(),
                                    v.getThumbnailUrl(),
                                    v.getCompleteUrl(),
                                    v.getDescription(),
                                    v.getTitle(),
                                    v.getLikesCount(),
                                    v.getHeight() == null ? 810 : v.getHeight(),
                                    v.getWidth() == null ? 810 : v.getWidth())).toList());
                        emitter.onComplete();
                    } else {
                        emitter.onComplete();
                    }
                }

                @Override
                public void onFailure(Call<VidmeVideosList> call, Throwable t) {
                    emitter.onComplete();
                    if (t instanceof UnknownHostException)
                        dataManager.onNetworkLost();
                }
            });
        });
    }

}
