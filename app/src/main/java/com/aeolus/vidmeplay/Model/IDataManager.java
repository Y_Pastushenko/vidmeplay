package com.aeolus.vidmeplay.Model;


import com.aeolus.vidmeplay.Model.DTO.VideoDTO;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Yuriy on 23.10.2017.
 */

public interface IDataManager {

    Observable<List<VideoDTO>> getFeaturedVideos(int offset, int limit);

    Observable<List<VideoDTO>> getNewVideos(int offset, int limit);

    Observable<List<VideoDTO>> getFollowedVideos(int offset, int limit);

    void logIn(String username, String password);

    void logOut();

    void onLoginSuccessful();

    void onInvalidUsername();

    void onInvalidPassword();

    void onNetworkLost();

    boolean isLoggedIn();

    boolean isNetworkAvailable();
}
