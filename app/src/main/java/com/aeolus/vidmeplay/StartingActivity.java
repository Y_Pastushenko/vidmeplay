package com.aeolus.vidmeplay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.aeolus.vidmeplay.Model.DataManager;
import com.aeolus.vidmeplay.Presenter.MainPagePresenter;
import com.aeolus.vidmeplay.View.MainPageActivity;

public class StartingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainPagePresenter presenter = MainPagePresenter.getInstance();
        DataManager dataManager = DataManager.getInstance(presenter);
        MainPageActivity.setPresenter(presenter);
        presenter.setDataManager(dataManager);
        startActivity(new Intent(this, MainPageActivity.class));
        this.finish();
    }
}
