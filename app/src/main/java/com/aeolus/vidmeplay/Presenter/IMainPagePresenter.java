package com.aeolus.vidmeplay.Presenter;

import android.content.Context;

import com.aeolus.vidmeplay.Model.IDataManager;
import com.aeolus.vidmeplay.View.IMainPageView;
import com.aeolus.vidmeplay.View.VO.VideoVO;

/**
 * Created by Yuriy on 23.10.2017.
 */

public interface IMainPagePresenter {

    enum Tabs {FEATURED, NEW, FEED}

    void setView(IMainPageView mainPageView);

    Context getContext();

    void onStart();

    void onVideoSelected(VideoVO video);

    void onMovedToTab(Tabs tab);

    void onSwipedTop();

    void onSwipedBottom();

    void onLogInPressed(String username, String password);

    void onLogoutPressed();

    void onLoginSuccessful();

    void onInvalidUsername();

    void onInvalidPassword();

    void onNetworkNotAvailable();

    void onRetryNetworkPressed();

}
