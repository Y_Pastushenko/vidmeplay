package com.aeolus.vidmeplay.Presenter;

import android.content.Context;

import com.aeolus.vidmeplay.Model.DTO.VideoDTO;
import com.aeolus.vidmeplay.Model.IDataManager;
import com.aeolus.vidmeplay.View.IMainPageView;
import com.aeolus.vidmeplay.View.VO.VideoVO;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

/**
 * Created by Yuriy on 25.10.2017.
 */

public class MainPagePresenter implements IMainPagePresenter {

    //<editor-fold desc="Fields">
    private static IDataManager dataManager;
    private static IMainPageView mainPageView;

    private static final MainPagePresenter instance = new MainPagePresenter();

    private static final int VIDEOS_PER_PAGE = 10;
    private ArrayList<String> shownVideoIDs;
    private HashMap<Tabs, Integer> offsets;
    private Tabs currentTab;
    //</editor-fold>

    private MainPagePresenter() {
    }

    public static MainPagePresenter getInstance() {
        return instance;
    }

    @Override
    public void setView(IMainPageView mainPageView) {
        MainPagePresenter.mainPageView = mainPageView;
    }

    public void setDataManager(IDataManager dataManager) {
        MainPagePresenter.dataManager = dataManager;
    }

    @Override
    public Context getContext() {
        return mainPageView.getContext();
    }

    @Override
    public void onStart() {
        if (!dataManager.isNetworkAvailable())
            mainPageView.showNoNetworkDialog(true);
        boolean isLoggedIn = dataManager.isLoggedIn();
        if (isLoggedIn)
            mainPageView.setFragmentType(IMainPageView.Tabs.FEED, IMainPageView.FragmentTypes.VIDEOS_LIST);
        mainPageView.setLoggedInView(isLoggedIn);
        offsets = new HashMap<>();
        offsets.put(Tabs.FEATURED, 0);
        offsets.put(Tabs.NEW, 0);
        offsets.put(Tabs.FEED, 0);
    }

    private void initCurrentVideosList() {
        offsets.put(currentTab, 0);
        if (shownVideoIDs !=null )
            shownVideoIDs = new ArrayList<>();
        Observer<List<VideoDTO>> observer = new Observer<List<VideoDTO>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<VideoDTO> videoDTOs) {
                if (videoDTOs == null)
                    videoDTOs = new ArrayList<>();
                updateCurrentVideosList(videoDTOs);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                mainPageView.showErrorLoadingVideosMessage();
            }

            @Override
            public void onComplete() {

            }
        };
        switch (currentTab) {
            case FEATURED:
                dataManager.getFeaturedVideos(0, VIDEOS_PER_PAGE).subscribe(observer);
                break;
            case NEW:
                dataManager.getNewVideos(0, VIDEOS_PER_PAGE).subscribe(observer);
                break;
            case FEED:
                dataManager.getFollowedVideos(0, VIDEOS_PER_PAGE).subscribe(observer);
                break;
        }
    }

    private void addNewVideosToCurrentVideosList(int amount) {
        Observer<List<VideoDTO>> observer = new Observer<List<VideoDTO>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<VideoDTO> videoDTOs) {
                if (videoDTOs == null)
                    videoDTOs = new ArrayList<>();
                offsets.put(currentTab, offsets.get(currentTab) + amount);
                List<VideoDTO> videos = new ArrayList<>();
                videos.addAll(videoDTOs);
                for (VideoDTO videoDTO : videoDTOs) {
                    if (shownVideoIDs == null)
                        shownVideoIDs = new ArrayList<>();
                    if (shownVideoIDs.contains(videoDTO.getVideoId()))
                        videos.remove(videoDTO);
                }
                appendToCurrentVideosList(videos);
                if (videos.size() != amount)
                    addNewVideosToCurrentVideosList(amount - videos.size());
            }

            @Override
            public void onError(@NonNull Throwable e) {
                mainPageView.showErrorLoadingVideosMessage();
            }

            @Override
            public void onComplete() {

            }
        };
        switch (currentTab) {
            case FEATURED:
                dataManager.getFeaturedVideos(offsets.get(currentTab), amount).subscribe(observer);
                break;
            case NEW:
                dataManager.getNewVideos(offsets.get(currentTab), amount).subscribe(observer);
                break;
            case FEED:
                dataManager.getFollowedVideos(offsets.get(currentTab), VIDEOS_PER_PAGE).subscribe(observer);
                break;
        }
    }

    @Override
    public void onVideoSelected(VideoVO video) {
        mainPageView.playVideo(video);
    }

    @Override
    public void onSwipedTop() {
        refreshCurrentVideosList();
    }

    private void refreshCurrentVideosList() {
        offsets.put(currentTab, 0);
        initCurrentVideosList();
    }

    @Override
    public void onSwipedBottom() {
        if (currentTab == Tabs.FEED && !dataManager.isLoggedIn())
            return;
        addNewVideosToCurrentVideosList(VIDEOS_PER_PAGE);
    }

    @Override
    public void onMovedToTab(Tabs tab) {
        currentTab = tab;
        if(mainPageView.isListEmpty()){
            offsets.put(currentTab,0);
            shownVideoIDs = new ArrayList<>();
        }
        if (currentTab == Tabs.FEED && !dataManager.isLoggedIn())
            return;
        if (offsets.get(currentTab) == 0)
            initCurrentVideosList();
    }

    private void appendToCurrentVideosList(List<VideoDTO> videos) {
        for (VideoDTO v : videos)
            shownVideoIDs.add(v.getVideoId());
        mainPageView.appendToList(dtoToVo(videos));
    }

    private void updateCurrentVideosList(List<VideoDTO> videos) {
        if (videos.size() == 0) {
            mainPageView.showNoVideosMessages();
            return;
        }
        if (shownVideoIDs == null)
            shownVideoIDs = new ArrayList<>();
        shownVideoIDs.addAll(Stream.of(videos).map(VideoDTO::getVideoId).toList());
        mainPageView.updateList(dtoToVo(videos));
    }

    @Override
    public void onLogInPressed(String username, String password) {
        dataManager.logIn(username, password);
    }

    @Override
    public void onLogoutPressed() {
        dataManager.logOut();
        mainPageView.setFragmentType(IMainPageView.Tabs.FEED, IMainPageView.FragmentTypes.LOGIN);
        mainPageView.setLoggedInView(false);
    }

    @Override
    public void onLoginSuccessful() {
        mainPageView.setFragmentType(IMainPageView.Tabs.FEED, IMainPageView.FragmentTypes.VIDEOS_LIST);
        mainPageView.setLoggedInView(true);
        if (currentTab == Tabs.FEED)
            onMovedToTab(currentTab);
    }

    @Override
    public void onInvalidUsername() {
        mainPageView.showInvalidUsernameMessage();
    }

    @Override
    public void onInvalidPassword() {
        mainPageView.showInvalidPasswordMessage();
    }

    @Override
    public void onNetworkNotAvailable() {
        mainPageView.showNoNetworkDialog(true);
    }

    @Override
    public void onRetryNetworkPressed() {
        if (dataManager.isNetworkAvailable()) {
            mainPageView.showNoNetworkDialog(false);
            if (shownVideoIDs == null || shownVideoIDs.size() == 0) {
                onStart();
                onMovedToTab(Tabs.FEATURED);
            }
        }
    }

    //<editor-fold desc="dto -> vo">
    private VideoVO dtoToVo(VideoDTO dto) {
        return new VideoVO(dto.getThumbnailUrl(),
                dto.getCompleteUrl(),
                dto.getDescription(),
                dto.getTitle(),
                dto.getLikesCount(),
                dto.getVideoHeight(),
                dto.getVideoWidth());
    }

    private List<VideoVO> dtoToVo(List<VideoDTO> videos) {
        return Stream.of(videos).map(video -> dtoToVo(video)).toList();
    }
    //</editor-fold>
}
