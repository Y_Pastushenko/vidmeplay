package com.aeolus.vidmeplay.View;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.aeolus.vidmeplay.Presenter.IMainPagePresenter;
import com.aeolus.vidmeplay.R;

/**
 * Created by Yuriy on 28.10.2017.
 */

public class NoNetworkAvailableDialogFragment extends DialogFragment {

    private static IMainPagePresenter presenter;
    private static final int BACK_PRESSED_COUNT_TO_CLOSE_APP = 2;
    private static int backPressedCount;

    Button retryButton;

    public static void setPresenter(IMainPagePresenter presenter) {
        NoNetworkAvailableDialogFragment.presenter = presenter;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.no_network_message)
                .setCancelable(false)
                .setNegativeButton(R.string.retry_button_text, null);
        AlertDialog dialog = builder.create();
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN)
                        return true;
                    backPressedCount++;
                    if (backPressedCount == BACK_PRESSED_COUNT_TO_CLOSE_APP - 1)
                        Toast.makeText(getActivity(), getString(R.string.press_back_to_exit_message), Toast.LENGTH_SHORT).show();
                    if (backPressedCount >= BACK_PRESSED_COUNT_TO_CLOSE_APP) {
                        backPressedCount = 0;
                        getActivity().finishAffinity();
                    }
                }
                return true;
            }
        });
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        backPressedCount = 0;
        final AlertDialog dialog = (AlertDialog) getDialog();
        if (dialog != null) {
            Button positiveButton = dialog.getButton(Dialog.BUTTON_NEGATIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onRetryNetworkPressed();
                }
            });
        }
    }
}
