package com.aeolus.vidmeplay.View;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.aeolus.vidmeplay.R;
import com.aeolus.vidmeplay.View.VO.VideoVO;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 25.10.2017.
 */

@SuppressWarnings("WeakerAccess")
public class VideoPlayerActivity extends AppCompatActivity {

    private static final String EXTRA_VIDEO = "extra_video";
    private SimpleExoPlayer sEPlayer;

    static class PreparePlayerTask extends AsyncTask<Void, Void, Void> {
        final HlsMediaSource videoSource;
        final SimpleExoPlayer sEPlayer;

        public PreparePlayerTask(HlsMediaSource videoSource, SimpleExoPlayer sEPlayer) {
            this.videoSource = videoSource;
            this.sEPlayer = sEPlayer;
        }

        @Override
        protected Void doInBackground(Void... params) {
            sEPlayer.prepare(videoSource);
            return null;
        }
    }

    @BindView(R.id.activity_video_player_player)
    SimpleExoPlayerView player;

    @BindView(R.id.activity_video_player_progress_bar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
        progressBar.setVisibility(View.VISIBLE);
        VideoVO video = getIntent().getParcelableExtra(EXTRA_VIDEO);
        setUpPlayer(Uri.parse(video.getCompleteUrl()), player);
        player.getPlayer().addVideoListener(new SimpleExoPlayer.VideoListener() {
            @Override
            public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

            }

            @Override
            public void onRenderedFirstFrame() {
                progressBar.setVisibility(View.GONE);
                player.getPlayer().removeVideoListener(this);
            }
        });
    }

    private void setUpPlayer(Uri uri, SimpleExoPlayerView playerView) {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        sEPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        playerView.setPlayer(sEPlayer);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, getResources().getString(R.string.app_name)));
        HlsMediaSource videoSource = new HlsMediaSource(uri, dataSourceFactory, null, null);
        PreparePlayerTask preparePlayerTask = new PreparePlayerTask(videoSource, sEPlayer);
        preparePlayerTask.execute();
        sEPlayer.setPlayWhenReady(true);
        playerView.setControllerAutoShow(false);
    }

    public static void startActivity(Activity activity, VideoVO video) {
        Intent intent = new Intent(activity, VideoPlayerActivity.class);
        intent.putExtra(EXTRA_VIDEO, video);
        activity.startActivity(intent);
    }

    @Override
    protected void onPause() {
        sEPlayer.release();
        super.onPause();
    }

}
