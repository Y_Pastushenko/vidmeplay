package com.aeolus.vidmeplay.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aeolus.vidmeplay.R;

/**
 * Created by Yuriy on 28.10.2017.
 */

public class ViewPagerPageFragment extends Fragment {

    private FragmentManager fm;
    private Fragment fragmentToAttach;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_pager_page, container, false);
        if (fm == null)
            fm = getChildFragmentManager();
        return view;
    }

    public void setFragment(Fragment newFragment) {
        fragmentToAttach = newFragment;
        if (isAdded()) {
            Fragment fragment = fm.findFragmentById(R.id.fragment_view_pager_page_frame_layout);
            if (fragment == null) {
                fm.beginTransaction().
                        setCustomAnimations(R.anim.fade_in, R.anim.fade_out).
                        add(R.id.fragment_view_pager_page_frame_layout, newFragment).
                        commit();
            } else {
                fm.beginTransaction().
                        setCustomAnimations(R.anim.fade_in, R.anim.fade_out).
                        replace(R.id.fragment_view_pager_page_frame_layout, newFragment).
                        commit();
            }
        }
    }

    public Fragment getFragment() {
        if (fragmentToAttach != null)
            return fragmentToAttach;
        return fm.findFragmentById(R.id.fragment_view_pager_page_frame_layout);
    }

    @Override
    public void onResume() {
        super.onResume();
        setFragment(fragmentToAttach);
    }

    public void onPageChanging() {
        if (getFragment() instanceof VideosListFragment)
            ((VideosListFragment) getFragment()).onPageChanging();
    }

}
