package com.aeolus.vidmeplay.View;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.aeolus.vidmeplay.Presenter.IMainPagePresenter;
import com.aeolus.vidmeplay.R;
import com.aeolus.vidmeplay.View.VO.VideoVO;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("WeakerAccess")
public class MainPageActivity extends AppCompatActivity implements IMainPageView {

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ViewPagerPageFragment page = new ViewPagerPageFragment();
            switch (new ArrayList<>(tabsTypeAndOrder.values()).get(position)) {
                case VIDEOS_LIST:
                    return getVideoListFragment(page);
                case LOGIN:
                    return getLoginFragment(page);
            }
            return page;
        }

        ViewPagerPageFragment getVideoListFragment(ViewPagerPageFragment page) {
            VideosListFragment fragment = new VideosListFragment();
            fragment.setPresenter(presenter);
            page.setFragment(fragment);
            return page;
        }

        ViewPagerPageFragment getLoginFragment(ViewPagerPageFragment page) {
            LoginFragment loginFragment = new LoginFragment();
            loginFragment.setPresenter(presenter);
            page.setFragment(loginFragment);
            return page;
        }

        @Override
        public int getCount() {
            return Tabs.values().length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return new ArrayList<>(tabsTypeAndOrder.keySet()).get(position).name();
        }
    }

    private static IMainPagePresenter presenter;
    private FragmentStatePagerAdapter pagerAdapter;
    private NoNetworkAvailableDialogFragment dialogFragment;
    private final LinkedHashMap<Tabs, FragmentTypes> tabsTypeAndOrder = new LinkedHashMap<>();

    private void setTabsTypeAndOrder() {
        tabsTypeAndOrder.put(Tabs.FEATURED, FragmentTypes.VIDEOS_LIST);
        tabsTypeAndOrder.put(Tabs.NEW, FragmentTypes.VIDEOS_LIST);
        tabsTypeAndOrder.put(Tabs.FEED, FragmentTypes.LOGIN);

    }

    @BindView(R.id.main_page_view_pager)
    ViewPager viewPager;

    @BindView(R.id.main_page_tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.main_page_actions_ib)
    ImageView actionsIB;

    public static void setPresenter(IMainPagePresenter presenter) {
        MainPageActivity.presenter = presenter;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main_page);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
        setTabsTypeAndOrder();
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                presenter.onMovedToTab(IMainPagePresenter.Tabs.valueOf(new ArrayList<>(tabsTypeAndOrder.keySet()).get(position).name()));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                getCurrentPage().onPageChanging();
            }
        });
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getCurrentPage().onPageChanging();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        actionsIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showActionsPopUpMenu(actionsIB);
            }
        });
        presenter.setView(this);
        presenter.onStart();
        presenter.onMovedToTab(IMainPagePresenter.Tabs.valueOf(new ArrayList<>(tabsTypeAndOrder.keySet()).get(0).name()));
    }

    @Override
    public void updateList(List<VideoVO> videos) {
        try {
            VideosListFragment listFragment = (VideosListFragment) getCurrentPage().getFragment();
            listFragment.updateList(videos);
        } catch (ClassCastException e) {

        }
    }

    @Override
    public void appendToList(List<VideoVO> videos) {
        try {
            VideosListFragment listFragment = (VideosListFragment) getCurrentPage().getFragment();
            listFragment.appendToList(videos);
        } catch (ClassCastException e) {

        }
    }

    @Override
    public void playVideo(VideoVO video) {
        VideoPlayerActivity.startActivity(this, video);
    }

    @Override
    public void showErrorLoadingVideosMessage() {
        makeMessage(getResources().getString(R.string.error_loading_video_message));
    }

    @Override
    public void showNoVideosMessages() {
        makeMessage(getString(R.string.no_videos_message));
    }

    @Override
    public void showNoNetworkDialog(boolean keep) {
        if (keep) {
            if (dialogFragment != null)
                dialogFragment.dismiss();
            dialogFragment = new NoNetworkAvailableDialogFragment();
            NoNetworkAvailableDialogFragment.setPresenter(presenter);
            dialogFragment.setCancelable(false);
            dialogFragment.show(getFragmentManager(), "no_network_dialog");
        } else
            dialogFragment.dismiss();
    }

    @Override
    public void setFragmentType(Tabs tab, FragmentTypes type) {
        if (tabsTypeAndOrder.get(tab) != type) {
            if (type == FragmentTypes.VIDEOS_LIST) {
                VideosListFragment fragment = new VideosListFragment();
                fragment.setPresenter(presenter);
                getPageByTab(tab).setFragment(fragment);
            } else if (type == FragmentTypes.LOGIN) {
                LoginFragment fragment = new LoginFragment();
                fragment.setPresenter(presenter);
                getPageByTab(tab).setFragment(fragment);
            }
        }
        tabsTypeAndOrder.put(tab, type);
    }

    @Override
    public void showInvalidUsernameMessage() {
        try {
            LoginFragment fragment = (LoginFragment) getPageByTab(Tabs.FEED).getFragment();
            fragment.cleanUsernameField();
            fragment.cleanPasswordField();
        } catch (ClassCastException e) {
        }
        makeMessage(getString(R.string.login_invalid_username_message));
    }

    @Override
    public void showInvalidPasswordMessage() {
        try {
            LoginFragment fragment = (LoginFragment) getPageByTab(Tabs.FEED).getFragment();
            fragment.cleanPasswordField();
        } catch (ClassCastException e) {
        }
        makeMessage(getString(R.string.login_invalid_password_message));
    }

    private void makeMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setLoggedInView(boolean set) {
        if (set)
            actionsIB.setVisibility(View.VISIBLE);
        else
            actionsIB.setVisibility(View.GONE);
    }

    @Override
    public boolean isListEmpty() {
        try {
            VideosListFragment fragment = (VideosListFragment) getCurrentPage().getFragment();
            return (fragment.getListCount() <= 0);
        } catch (ClassCastException e) {
            return true;
        }
    }

    private void showActionsPopUpMenu(View attachTo) {
        PopupMenu popupMenu = new PopupMenu(this, attachTo);
        popupMenu.inflate(R.menu.actions_pop_up_menu);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.actions_pop_up_menu_logout_item:
                        presenter.onLogoutPressed();
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    private ViewPagerPageFragment getPageByTab(Tabs tab) {
        return (ViewPagerPageFragment) pagerAdapter.instantiateItem(viewPager, new ArrayList<>(tabsTypeAndOrder.keySet()).indexOf(tab));
    }

    private ViewPagerPageFragment getCurrentPage() {
        return (ViewPagerPageFragment) pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem());
    }

}
