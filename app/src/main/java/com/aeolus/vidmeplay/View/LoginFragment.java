package com.aeolus.vidmeplay.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aeolus.vidmeplay.Presenter.IMainPagePresenter;
import com.aeolus.vidmeplay.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 27.10.2017.
 */

@SuppressWarnings("WeakerAccess")
public class LoginFragment extends Fragment {

    @BindView(R.id.fragment_login_username_et)
    EditText usernameET;

    @BindView(R.id.fragment_login_password_et)
    EditText passwordET;

    @BindView(R.id.fragment_login_login_button)
    Button loginButton;

    private IMainPagePresenter presenter;

    public void setPresenter(IMainPagePresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (usernameET.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), getString(R.string.empty_username_field_message), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (passwordET.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), getString(R.string.empty_password_field_message), Toast.LENGTH_SHORT).show();
                    return;
                }
                presenter.onLogInPressed(usernameET.getText().toString(), passwordET.getText().toString());
            }
        });
        return view;
    }

    public void cleanUsernameField() {
        usernameET.setText("");
    }

    public void cleanPasswordField() {
        passwordET.setText("");
    }
}
