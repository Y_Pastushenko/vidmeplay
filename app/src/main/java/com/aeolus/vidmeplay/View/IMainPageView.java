package com.aeolus.vidmeplay.View;

import android.content.Context;

import com.aeolus.vidmeplay.View.VO.VideoVO;

import java.util.List;

/**
 * Created by Yuriy on 23.10.2017.
 */

public interface IMainPageView {

    enum Tabs {FEATURED, NEW, FEED}

    enum FragmentTypes {VIDEOS_LIST, LOGIN}

    Context getContext();

    void setFragmentType(Tabs tab, FragmentTypes type);

    void updateList(List<VideoVO> videos);

    void appendToList(List<VideoVO> videos);

    void playVideo(VideoVO video);

    void showErrorLoadingVideosMessage();

    void showNoVideosMessages();

    void showInvalidUsernameMessage();

    void showInvalidPasswordMessage();

    void setLoggedInView(boolean set);

    void showNoNetworkDialog(boolean keep);

    boolean isListEmpty();

}
