package com.aeolus.vidmeplay.View;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aeolus.vidmeplay.Presenter.IMainPagePresenter;
import com.aeolus.vidmeplay.R;
import com.aeolus.vidmeplay.View.VO.VideoVO;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 23.10.2017.
 */

@SuppressWarnings("WeakerAccess")
public class VideosListFragment extends Fragment {

    private static IMainPagePresenter presenter;

    @BindView(R.id.fragment_video_list_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.fragment_video_list_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;


    private List<VideoVO> videos;
    private static final boolean PLAYING_VIDEO_AT_CENTRE = true;
    private static final int LIST_ITEM_BACKGROUND_COLOR = R.color.colorDialog;
    private static final int SIDE_STROKES_WIDTH_DP = 3;

    private boolean playBackAllowed = false;
    private int currentCentralPosition;
    private boolean swipedBottom = false;

    private SimpleExoPlayer player;

    static class PreparePlayerTask extends AsyncTask<Void, Void, Void> {
        final HlsMediaSource videoSource;
        final SimpleExoPlayer sEPlayer;

        private PreparePlayerTask(HlsMediaSource videoSource, SimpleExoPlayer sEPlayer) {
            this.videoSource = videoSource;
            this.sEPlayer = sEPlayer;
        }

        @Override
        protected Void doInBackground(Void... params) {
            sEPlayer.prepare(videoSource);
            return null;
        }
    }

    private class VideoHolder extends RecyclerView.ViewHolder {
        final ImageView image;
        final SimpleExoPlayerView playerView;
        final TextView title;
        final TextView likesCount;
        final ProgressBar imageProgressBar;
        final ProgressBar playerProgressBar;

        private VideoHolder(View viewItem) {
            super(viewItem);
            image = viewItem.findViewById(R.id.videos_list_item_video_picture);
            playerView = viewItem.findViewById(R.id.videos_list_item_video_player);
            playerView.setControllerAutoShow(false);
            title = viewItem.findViewById(R.id.videos_list_item_video_title);
            likesCount = viewItem.findViewById(R.id.videos_list_item_video_likes_count);
            imageProgressBar = viewItem.findViewById(R.id.videos_list_item_image_progress_bar);
            playerProgressBar = viewItem.findViewById(R.id.videos_list_item_player_progress_bar);
            int resId = getResources().getIdentifier("exo_shutter", "id", getActivity().getPackageName());
            viewItem.findViewById(resId).setBackground(new ColorDrawable(getResources().getColor(LIST_ITEM_BACKGROUND_COLOR)));
        }
    }

    private class VideoAdapter extends RecyclerView.Adapter<VideoHolder> {
        final List<VideoVO> videos;

        VideoAdapter(List<VideoVO> videos) {
            this.videos = videos;
        }

        @Override
        public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(R.layout.videos_list_item, parent, false);
            return new VideoHolder(view);
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public void onBindViewHolder(VideoHolder holder, int position) {
            VideoVO video = videos.get(position);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int strokes = (int) Math.ceil(SIDE_STROKES_WIDTH_DP * displayMetrics.density);
            double scale = (double) video.getVideoWidth() / (displayMetrics.widthPixels - strokes);
            int height = (int) Math.round(video.getVideoHeight() / scale);
            if (PLAYING_VIDEO_AT_CENTRE && playBackAllowed && position == currentCentralPosition) {
                holder.imageProgressBar.setVisibility(View.GONE);
                holder.playerProgressBar.setVisibility(View.VISIBLE);
                holder.image.setVisibility(View.GONE);
                holder.playerView.setVisibility(View.VISIBLE);
                holder.playerView.getLayoutParams().width = displayMetrics.widthPixels - strokes;
                holder.playerView.getLayoutParams().height = height;
                setUpPlayer(Uri.parse(videos.get(position).getCompleteUrl()), holder.playerView);
                holder.playerView.getPlayer().addVideoListener(new SimpleExoPlayer.VideoListener() {
                    @Override
                    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

                    }

                    @Override
                    public void onRenderedFirstFrame() {
                        holder.playerProgressBar.setVisibility(View.GONE);
                        holder.playerView.getPlayer().removeVideoListener(this);
                    }
                });
                final GestureDetector gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
                    public void onLongPress(MotionEvent e) {
                        onVideoSelected(video);
                    }
                });
                holder.playerView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        gestureDetector.onTouchEvent(motionEvent);
                        view.performClick();
                        return false;
                    }
                });

            } else {
                holder.imageProgressBar.setVisibility(View.VISIBLE);
                holder.playerProgressBar.setVisibility(View.GONE);
                BitmapDrawable placeHolder = createBitmap(video.getVideoWidth(), height, getResources().getColor(LIST_ITEM_BACKGROUND_COLOR));
                holder.playerView.setVisibility(View.GONE);
                holder.image.setVisibility(View.VISIBLE);
                holder.image.layout(0, 0, 0, 0);
                Glide.with(getContext())
                        .load(videos.get(position).getThumbnailUrl())
                        .apply(new RequestOptions().placeholder(placeHolder))
                        .apply(new RequestOptions().override(displayMetrics.widthPixels - strokes, height).centerCrop())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
                                holder.imageProgressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.image);
                holder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onVideoSelected(video);
                    }
                });
            }
            holder.title.setText(video.getTitle());
            int i = video.getLikesCount();
            holder.likesCount.setText(getResources().getQuantityString(R.plurals.likes_string, i, i));
        }

        void setUpPlayer(Uri uri, SimpleExoPlayerView playerView) {
            if (player != null)
                player.release();
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            player = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector);
            playerView.setPlayer(player);
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getContext(), Util.getUserAgent(getContext(), getResources().getString(R.string.app_name)));
            HlsMediaSource videoSource = new HlsMediaSource(uri, dataSourceFactory, null, null);
            PreparePlayerTask preparePlayerTask = new PreparePlayerTask(videoSource, player);
            preparePlayerTask.execute();
            player.setPlayWhenReady(true);
        }

        @Override
        public int getItemCount() {
            return videos.size();
        }

        BitmapDrawable createBitmap(int width, int height, int color) {
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);

            Paint paint = new Paint();
            paint.setColor(color);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawPaint(paint);
            return new BitmapDrawable(getResources(), bitmap);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_videos_list, container, false);
        ButterKnife.bind(this, view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    if (!swipedBottom) {
                        presenter.onSwipedBottom();
                        swipedBottom = true;
                    }
                }
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.onSwipedTop();
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        onScrollStarted();
                        break;
                    case RecyclerView.SCROLL_STATE_IDLE:
                        onScrollStopped();
                        break;
                }
            }
        });
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissPlayer();
            }
        });
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null) {
            if (adapter.getItemCount() == 0)
                swipeRefreshLayout.setRefreshing(true);
        }
        swipeRefreshLayout.setRefreshing(true);
        return view;
    }

    @Override
    public void onPause() {
        dismissPlayer();
        super.onPause();
    }

    public void updateList(List<VideoVO> videos) {
        if (this.videos == null)
            this.videos = new ArrayList<>();
        this.videos.clear();
        this.videos.addAll(videos);
        VideoAdapter adapter = new VideoAdapter(this.videos);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);
        swipedBottom = false;
    }

    public void appendToList(List<VideoVO> videos) {
        if (videos == null)
            videos = new ArrayList<>();
        if (this.videos == null)
            this.videos = new ArrayList<>();
        int start = this.videos.size();
        this.videos.addAll(videos);
        int end = this.videos.size();
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null)
            adapter.notifyItemRangeInserted(start, end);
        swipedBottom = false;
    }

    public void setPresenter(IMainPagePresenter presenter) {
        VideosListFragment.presenter = presenter;
    }

    private void onVideoSelected(VideoVO video) {
        dismissPlayer();
        presenter.onVideoSelected(video);
    }

    private void onScrollStopped() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        currentCentralPosition = layoutManager.findFirstVisibleItemPosition() + (layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition()) / 2;
        playBackAllowed = true;
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null)
            adapter.notifyItemChanged(currentCentralPosition);
    }

    private void onScrollStarted() {
        dismissPlayer();
    }

    private void dismissPlayer() {
        if (player != null)
            player.release();
        playBackAllowed = false;
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null)
            adapter.notifyItemChanged(currentCentralPosition);
    }

    public void onPageChanging() {
        dismissPlayer();
    }

    public int getListCount() {
        if (recyclerView == null)
            return 0;
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter == null)
            return 0;
        return adapter.getItemCount();
    }
}
