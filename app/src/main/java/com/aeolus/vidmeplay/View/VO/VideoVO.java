package com.aeolus.vidmeplay.View.VO;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Yuriy on 25.10.2017.
 */

public class VideoVO implements Parcelable {
    private final String thumbnailUrl;
    private final String completeUrl;
    private final String description;
    private final String title;
    private final Integer likesCount;
    private final Integer videoHeight;
    private final Integer videoWidth;

    public VideoVO(String thumbnailUrl, String completeUrl, String description, String title, Integer likesCount, Integer videoHeight, Integer videoWidth) {
        this.thumbnailUrl = thumbnailUrl;
        this.completeUrl = completeUrl;
        this.description = description;
        this.title = title;
        this.likesCount = likesCount;
        this.videoHeight = videoHeight;
        this.videoWidth = videoWidth;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getCompleteUrl() {
        return completeUrl;
    }

    public String getTitle() {
        return title;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public Integer getVideoHeight() {
        return videoHeight;
    }

    public Integer getVideoWidth() {
        return videoWidth;
    }

    private VideoVO(Parcel in) {
        thumbnailUrl = in.readString();
        completeUrl = in.readString();
        description = in.readString();
        title = in.readString();
        likesCount = in.readByte() == 0x00 ? null : in.readInt();
        videoHeight = in.readByte() == 0x00 ? null : in.readInt();
        videoWidth = in.readByte() == 0x00 ? null : in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(thumbnailUrl);
        dest.writeString(completeUrl);
        dest.writeString(description);
        dest.writeString(title);
        if (likesCount == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(likesCount);
        }
        if (videoHeight == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(videoHeight);
        }
        if (videoWidth == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(videoWidth);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<VideoVO> CREATOR = new Parcelable.Creator<VideoVO>() {
        @Override
        public VideoVO createFromParcel(Parcel in) {
            return new VideoVO(in);
        }

        @Override
        public VideoVO[] newArray(int size) {
            return new VideoVO[size];
        }
    };
}